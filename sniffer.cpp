# include "sniffer.h"

pcap_t* pd;
int linkhdrlen;

pcap_t* open_pcap_socket(char* interface, const char* bpfstr)
{
	pcap_t* pd;
	char errbuf[PCAP_ERRBUF_SIZE];
	uint32_t srcip, netmask;
	struct bpf_program bpf;

	// If no network interface are specified, get the first one
	if(!*interface && !(interface = pcap_lookupdev(errbuf)))
	{
		cout<<"\npcap_lookupdev(): "<<errbuf;
		return NULL;
	}

	// Opening device for live capture
	if((pd = pcap_open_live(interface, BUFSIZ, 1, 0, errbuf)) == NULL)
	{
		cout<<"\npcap_open_live(): "<<errbuf;
		return NULL;
	}

	// Getting network device source IP address and netmask.
	if (pcap_lookupnet(interface, &srcip, &netmask, errbuf) < 0)
	{
		cout<<"\npcap_lookupnet: "<<errbuf;
		return NULL;
	}

	// Converting the packet filter expression into a packet filter
	if (pcap_compile(pd, &bpf, (char*)bpfstr, 0, netmask))
	{
		cout<<"\npcap_compile(): "<<pcap_geterr(pd);
		return NULL;
	}

	// Assigning the packet filter to the given libpcap socket.
	if (pcap_setfilter(pd, &bpf) < 0)
	{
		cout<<"\npcap_setfilter(): "<<pcap_geterr(pd);
		return NULL;
	}

	return pd;
}

void bailout(int signo)
{
	struct pcap_stat stats;

	if (pcap_stats(pd, &stats) >= 0)
	{
		cout<<"\nPackets received: "<<stats.ps_recv<<endl;
		cout<<"\nPackets dropped: "<<stats.ps_drop<<endl;
	}
	pcap_close(pd);
	exit(0);
}

void parse_packet(u_char *user, struct pcap_pkthdr *packethdr, u_char *packetptr)
{

	struct ip* iphdr;
	struct icmphdr* icmphdr;
	struct tcphdr* tcphdr;
	struct udphdr* udphdr;

	ostringstream iphdrInfo;
	char srcip[256], dstip[256];
	unsigned short id, seq;

	// Skipping the datalink layer header and get the IP header fields.
	packetptr += linkhdrlen;
	iphdr = (struct ip*)packetptr;
	strcpy(srcip, inet_ntoa(iphdr -> ip_src));
	strcpy(dstip, inet_ntoa(iphdr -> ip_dst));
	iphdrInfo<< "ID: "<<ntohs(iphdr->ip_id)<<" IpLen: "<<
			4*iphdr->ip_hl<<" DgLen: "<<ntohs(iphdr->ip_len);

	// Advance to the transport layer header then parse and display
	// the fields based on the type of hearder: tcp, udp or icmp.
	packetptr += 4*iphdr->ip_hl;
	switch (iphdr->ip_p)
	{
	case IPPROTO_TCP:
		tcphdr = (struct tcphdr*)packetptr;
		cout<<"\n+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-";
		cout<<"\nProtocol		Src IP		Src Port		Dest IP		Dest Port";
		cout<<"\nTCP 	"<<srcip<<" 	"<<ntohs(tcphdr -> source)<<" 	"
				<<dstip<<" 		"<<ntohs(tcphdr -> dest);
		cout<<"\n"<<iphdrInfo.str();
		cout<<"Seq: "<<ntohl(tcphdr -> seq)<<" Ack: "<<ntohl(tcphdr -> ack_seq);
		cout<<"\nFlags: "<<(tcphdr->urg ? 'U' : '*')<<(tcphdr->ack ? 'A' : '*')<<(tcphdr->psh ? 'P' : '*')<<
				(tcphdr->rst ? 'R' : '*')<<(tcphdr->syn ? 'S' : '*')<<(tcphdr->fin ? 'F' : '*');
		cout<<"\n+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-";
		break;

	case IPPROTO_UDP:
		udphdr = (struct udphdr*)packetptr;
		cout<<"\n+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-";
		cout<<"\nProtocol		Src IP		Src Port		Dest IP		Dest Port";
		cout<<"\nUDP  	"<<srcip<<" 	"<<ntohs(udphdr->source)<<" 	"<<
				dstip<<" 	"<<ntohs(udphdr->dest);
		cout<<"\n"<<iphdrInfo.str();
		cout<<"\n+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-";
		break;

	case IPPROTO_ICMP:
		icmphdr = (struct icmphdr*)packetptr;
		cout<<"\n+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-";
		cout<<"\nProtocol		Src IP		Dest IP";
		cout<<"\nICMP "<<srcip<<" "<<dstip;
		cout<<"\n"<<iphdrInfo.str();
		//memcpy(&id, (u_char*)icmphdr+4, sizeof((u_char*)icmphdr+4));
		//memcpy(&seq, (u_char*)icmphdr+6, sizeof((u_char*)icmphdr+6));
		cout<<"\nType: "<<icmphdr->type<<" Code: "<<icmphdr->code<<" ID: "<<ntohs(id)<<" Seq: "<<ntohs(seq);
		cout<<"\n+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-";
		break;
	}
}

void capture_loop(pcap_t* pd, int packets, pcap_handler func)
{
	int linktype;

	// Determining the datalink layer type.
	if ((linktype = pcap_datalink(pd)) < 0)
	{
		cout<<"\npcap_datalink(): "<<pcap_geterr(pd);
		return;
	}

	// Setting the datalink layer header size.
	switch (linktype)
	{
	case DLT_NULL:

		linkhdrlen = 4;
		break;

	case DLT_EN10MB:
		linkhdrlen = 14;
		break;

	case DLT_SLIP:
	case DLT_PPP:
		linkhdrlen = 24;
		break;

	default:
		cout<<"\nUnsupported datalink: "<<linktype;
		return;
	}

	// Starting to capture packets.
	if (pcap_loop(pd, packets, func, 0) < 0)
	{
		cout<<"\npcap_loop failed: "<<pcap_geterr(pd);
	}
}

void pktCapfile(int, int, char*);

int main(int argc, char **argv)
{
	system("clear");

	char interface[256] = "";   // -i interface
	char bpfstr[256] = "";
	int t = 0;   // -t time, -n number of packets
	int offset = 0;   // -o offset
	char *fileWS = '\0';  // -f filename
	int opt, nPackets, i;
	while ((opt = getopt (argc, argv, "f:i:t:o:n:")) != -1)
	{
		switch (opt)
		{
		case 'i':
			strcpy(interface,optarg);
			break;
		case 't':
			t = atoi(optarg);
			break;
		case 'o':
			offset = atoi(optarg);
			break;
		case 'n':
			nPackets = atoi(optarg);
			break;
		case 'f':
			fileWS = optarg;
			break;
		default:
			cout<<"\nParameters can't be read..!!";
			abort();
		}
	}

	if (fileWS != '\0')
	{
		cout<<"\n=================================================";
		cout<<"\nAnalyzing packet from file..!!"<<fileWS;
		cout<<"\nTime: "<<t;
		cout<<"\nOff-set: "<<offset<<endl;
		usleep(10 * 100 * 1000);
		pktCapfile(t, offset, fileWS);
	}
	else
	{
		cout<<"\n=================================================";
		cout<<"\nAnalyzing live packet..!!";
		cout<<"\nTime: "<<t;
		cout<<"\nInterface: "<<interface<<endl;
		usleep(10 * 100 * 1000);
		for (i = optind; i < argc; i++)
		{
			strcpy(bpfstr, argv[i]);
			strcpy(bpfstr," ");
		}
		/*
					cout<<"\nCapturing packet from live interface..!!";
					cout<<"\nTime: "<<t;
					cout<<"\nInterface: "<<interface<<endl;
		*/
		if ((pd = open_pcap_socket(interface, bpfstr)))
		{
			//cout<<pd;
			signal(SIGINT, bailout);
			signal(SIGTERM, bailout);
			signal(SIGQUIT, bailout);
			capture_loop(pd, nPackets, (pcap_handler)parse_packet);

			bailout(0);
		}
		exit(0);
	}
	return 0;
}

void pktCapfile(int t, int offset, char *fileWS)				// To analyze packets from 'pcap' file
{
	char errbuff[PCAP_ERRBUF_SIZE];
	pcap_t * handler = pcap_open_offline(fileWS, errbuff);
	struct pcap_pkthdr *header;
	const u_char *packet;
	int packetCount = 0;
	int i = 0;

	/*
	struct ip* iphdr;
	struct icmphdr* icmphdr;
	struct tcphdr* tcphdr;
	struct udphdr* udphdr;*/

	const struct sniff_ethernet *ethernet;
	const struct sniff_ip *ip;
	const struct sniff_tcp *tcp;
	//const struct sniff_udp *udp;
	//const struct sniff_icmp *icmp;
	u_int size_ip;
	//u_int size_tcp;
	//u_int size_udp;
	//u_int size_icmp;

	while(pcap_next_ex(handler, &header, &packet) >= 0)
	{
		cout<<"\n-------------------------------------------------------------------------";
		cout<<"\nPacket #: "<<++packetCount;
		ethernet = (struct sniff_ethernet*)(packet);
		ip = (struct sniff_ip*)(packet + SIZE_ETHERNET);
		size_ip = IP_HL(ip)*4;

		tcp = (struct sniff_tcp*)(packet + SIZE_ETHERNET + size_ip);
		cout<<"\nProtocol	Source IP	Source Port	Destn. IP	Destn. Port";
		cout<<"\nTCP	"<<inet_ntoa(ip -> ip_src)<<"	"<<ntohs(tcp -> th_sport)<<"	"<<
				inet_ntoa(ip->ip_dst)<<"	"<<ntohs(tcp->th_dport);
		cout<<"\nSeq no: "<<(unsigned int)tcp-> th_seq<<"	Ack No: "<<(unsigned int)tcp->th_ack;
		cout<<"\nPacket SIZE: "<<header -> len<<" bytes";
		cout<<"\nEpoch Time: "<<header -> ts.tv_sec<<" Secs: "<<header -> ts.tv_usec;
		cout<<"\n-------------------------------------------------------------------------"<<endl;
		/*
		case IPPROTO_UDP:
			cout<<"\n-------------------------------------------------------------------------";
			cout<<"\nPacket #: "<<++packetCount;
			ethernet = (struct sniff_ethernet*)(packet);
			ip = (struct sniff_ip*)(packet + SIZE_ETHERNET);
			size_ip = IP_HL(ip)*4;

			udp = (struct sniff_udp*)(packet + SIZE_ETHERNET + size_ip);
			cout<<"\nProtocol  Source IP  Source Port  Destn. IP  Destn. Port";
			cout<<"\nUDP	"<<inet_ntoa(ip -> ip_src)<<"	"<<ntohs(udp -> th_sport)<<"	"
					<<inet_ntoa(ip -> ip_dst)<<"	"<<ntohs(udp -> th_dport);
			cout<<"\nPacket SIZE: "<<header -> len<<" bytes";
			cout<<"\nEpoch Time: "<<header -> ts.tv_sec<<" Sec: "<<header -> ts.tv_usec;
			cout<<"\n-------------------------------------------------------------------------";
			break;

		case IPPROTO_ICMP:
			cout<<"\n-------------------------------------------------------------------------";
			cout<<"\nPacket #: "<<++packetCount;
			ethernet = (struct sniff_ethernet*)(packet);
			ip = (struct sniff_ip*)(packet + SIZE_ETHERNET);
			size_ip = IP_HL(ip)*4;

			icmp = (struct sniff_icmp*)(packet + SIZE_ETHERNET + size_ip);
			cout<<"\nProtocol  Source IP  Source Port  Destn. IP  Destn. Port";
			cout<<"\nICMP	"<<inet_ntoa(ip -> ip_src)<<"	"<<ntohs(udp -> th_sport)<<"	"
					<<inet_ntoa(ip -> ip_dst)<<"	"<<ntohs(udp -> th_dport);
			cout<<"\nPacket SIZE: "<<header -> len<<" bytes";
			cout<<"\nEpoch Time: "<<header -> ts.tv_sec<<" Sec: "<<header -> ts.tv_usec;
			cout<<"\n-------------------------------------------------------------------------";
			break;
			*/
	}
}
