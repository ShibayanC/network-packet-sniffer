OBJS = sniffer.o
CC = g++
DEBUG = -g
CFLAGS = -Wall -c $(DEBUG)
LFLAG = -Wall $(DEBUG)

h1 : $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o sniffer -lpcap

sniffer.o: sniffer.h sniffer.cpp
	$(CC) $(CFLAGS) sniffer.cpp

clean:
	-rm -f h1 *.o 

tar:
	tar cfv h1.shibayan.tar sniffer.h sniffer.cpp makefile README
